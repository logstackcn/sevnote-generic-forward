# README #

Sevnote Generic Forward 通用数据转发和接收器



### 安装使用 ###

* 安装nodejs环境,请访问www.nodejs.org
* git clone https://bitbucket.org/logstackcn/sevnote-generic-forward/
* 修改配置文件
vi etc/sevnote-forward.json
* 添加需要转发的数据源,需要设定目标服务器地址，默认为access1.sevnote.com
vi config/config.conf
* 运行
./bin/sevnote-generic-forward --config_dir=/root/sevnote-generic-forward/config/


### 授权许可 ###

* www.sevnote.com
* apache  licence
